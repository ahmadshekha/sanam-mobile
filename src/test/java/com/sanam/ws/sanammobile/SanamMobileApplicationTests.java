package com.sanam.ws.sanammobile;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SanamMobileApplicationTests {

    @Test
    public void contextLoads() {
        List<Integer> ints = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        ints.stream()
                .forEach(i -> {
                    if (i.intValue() % 2 == 0) {
                        Assert.assertTrue(i.intValue() % 2 == 0);
                    } else {
                        Assert.assertTrue(i.intValue() % 2 != 0);
                    }
                });

        Stream<Integer> oddIntegers =   ints.stream().filter(i -> i.intValue() % 2 != 0);
        Stream<Integer> evenIntegers =   ints.stream().filter(i -> i.intValue() % 2 == 0);

        evenIntegers.forEach(i -> Assert.assertTrue(i.intValue() % 2 == 0));
        oddIntegers.forEach(i -> Assert.assertTrue(i.intValue() % 2 != 0));
    }

}
