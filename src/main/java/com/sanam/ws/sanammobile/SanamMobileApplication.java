package com.sanam.ws.sanammobile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
@EntityScan(basePackageClasses = {SanamMobileApplication.class, Jsr310JpaConverters.class})
public class SanamMobileApplication {

    @PostConstruct
    void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+3"));
    }

    /**
     * main method to create Spring boot ApplicationContext.
     * <p>
     * The SpringApplication#run() method will do following:
     * <p>
     * 1- Create an appropriate ApplicationContext instance (depending on your classpath)
     * 2- Register a CommandLinePropertySource to expose command line arguments as Spring properties
     * 3- Refresh the application context, loading all singleton beans
     * 4- Trigger any CommandLineRunner beans
     *
     * @param args arguments
     */
    public static void main(final String[] args) {
        SpringApplication.run(SanamMobileApplication.class, args);
    }
}
