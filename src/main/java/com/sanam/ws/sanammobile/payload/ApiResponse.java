package com.sanam.ws.sanammobile.payload;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.Date;
import java.util.List;


/**
 * This response is the response wrapper class for all kind of web services
 *
 * @author Salah Abu Msameh
 */
@JsonRootName("response")
@JsonInclude(Include.NON_NULL)
public class ApiResponse<R> {

    private String responseStatus;
    private Date date;
    private String errorCode;
    private String errorMessage;
    private String errorDetails;
    private R responseBody;
    private List<ValidationError> validationErrors;

    public String getResponseStatus() {
        return responseStatus;
    }

    @JsonProperty("response_status")
    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    @JsonProperty("date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @JsonProperty("error_code")
    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @JsonProperty("error_message")
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @JsonProperty("error_details")
    public String getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(String errorDetails) {
        this.errorDetails = errorDetails;
    }

    @JsonProperty("response_body")
    public R getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(R responseBody) {
        this.responseBody = responseBody;
    }

    @JsonProperty("validation_errors")
    public List<ValidationError> getValidationErrors() {
        return validationErrors;
    }

    public void setValidationErrors(List<ValidationError> validationErrors) {
        this.validationErrors = validationErrors;
    }
}
