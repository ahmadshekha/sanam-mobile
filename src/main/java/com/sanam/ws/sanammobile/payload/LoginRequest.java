package com.sanam.ws.sanammobile.payload;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;

/**
 * Login API Request.
 *
 * @author Ahmad Shekha
 * @since 4/3/2020
 */
public class LoginRequest {

    @JsonProperty("username_or_email")
    @NotBlank(message = "username_or_email field is mandatory")
    private String usernameOrEmail;

    @JsonProperty("password")
    @NotBlank(message = "Password field is mandatory")
    private String password;

    public String getUsernameOrEmail() {
        return usernameOrEmail;
    }

    public void setUsernameOrEmail(final String usernameOrEmail) {
        this.usernameOrEmail = usernameOrEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }
}
