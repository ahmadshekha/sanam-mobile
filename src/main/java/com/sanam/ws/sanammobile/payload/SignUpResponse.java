package com.sanam.ws.sanammobile.payload;

import com.sanam.ws.sanammobile.dao.entity.User;

/**
 * @author Ahmad Shekha
 * @since 4/11/2020
 */
public class SignUpResponse {

    private User user;

    public SignUpResponse(final User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(final User user) {
        this.user = user;
    }
}
