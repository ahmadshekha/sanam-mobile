package com.sanam.ws.sanammobile.payload;

import com.sanam.ws.sanammobile.dao.entity.Mobile;

/**
 * @author Ahmad Shekha
 * @since 4/11/2020
 */
public class MobileResponse {

    private Mobile mobile;

    public MobileResponse(final Mobile mobile) {
        this.mobile = mobile;
    }

    public Mobile getMobile() {
        return mobile;
    }

    public void setMobile(final Mobile mobile) {
        this.mobile = mobile;
    }
}
