package com.sanam.ws.sanammobile.payload;

import com.sanam.ws.sanammobile.dao.entity.Cover;

/**
 * @author Ahmad Shekha
 * @since 4/13/2020
 */
public class AddCoverResponse {

    private final Cover cover;

    public AddCoverResponse(final Cover cover) {
        this.cover = cover;
    }

    public Cover getCover() {
        return cover;
    }
}
