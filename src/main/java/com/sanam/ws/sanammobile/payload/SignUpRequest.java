package com.sanam.ws.sanammobile.payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sanam.ws.sanammobile.validator.ValidEmail;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Ahmad Shekha
 * @since 4/11/2020
 */
public class SignUpRequest {

    @NotBlank(message = "first_name is mandatory")
    @Size(max = 40)
    @JsonProperty("first_name")
    private String firstName;

    @NotBlank(message = "last_name is mandatory")
    @Size(max = 40)
    @JsonProperty("last_name")
    private String lastName;

    @NotBlank(message = "username is mandatory")
    @Size(min = 5, max = 15)
    @JsonProperty("username")
    private String username;

    @NotBlank(message = "email is mandatory")
    @Size(max = 40)
    @ValidEmail
    @JsonProperty("email")
    private String email;

    @NotBlank(message = "password is mandatory")
    @Size(min = 6, max = 20)
    @JsonProperty("password")
    private String password;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
