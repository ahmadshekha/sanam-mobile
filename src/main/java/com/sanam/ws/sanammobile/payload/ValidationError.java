package com.sanam.ws.sanammobile.payload;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ahmad Shekha
 * @since 4/7/2020
 */
public class ValidationError {

    private String fieldName;
    private String fieldError;

    /**
     * Zero-argument constructor
     */
    public ValidationError() {
    }

    /**
     *
     * @param fieldName
     * @param fieldError
     */
    public ValidationError(final String fieldName, final String fieldError) {
        this.fieldName = fieldName;
        this.fieldError = fieldError;
    }

    @JsonProperty("field_name")
    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(final String fieldName) {
        this.fieldName = fieldName;
    }

    @JsonProperty("field_error")
    public String getFieldError() {
        return fieldError;
    }

    public void setFieldError(final String fieldError) {
        this.fieldError = fieldError;
    }
}
