package com.sanam.ws.sanammobile.payload;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ahmad Shekha
 * @since 4/16/2020
 */
public class EditMobileRequest {

    private long mobileId;
    private String type;
    private int quantity;
    private long category;

    @JsonProperty("mobile_id")
    public long getMobileId() {
        return mobileId;
    }

    public void setMobileId(final long mobileId) {
        this.mobileId = mobileId;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(final int quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("category_id")
    public long getCategory() {
        return category;
    }

    public void setCategory(final long category) {
        this.category = category;
    }
}
