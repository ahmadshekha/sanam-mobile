package com.sanam.ws.sanammobile.payload;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author Ahmad Shekha
 * @since 4/13/2020
 */
public class AddCoverRequest {

    @NotBlank
    @Size(max = 45)
    private String type;

    @Min(value = 1, message = "must be equal or greater than 1")
    private int quantity;

    @Min(value = 1, message = "must be equal or greater than 1")
    private int category;

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(final int quantity) {
        this.quantity = quantity;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(final int category) {
        this.category = category;
    }
}
