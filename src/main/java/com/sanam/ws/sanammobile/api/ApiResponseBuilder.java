package com.sanam.ws.sanammobile.api;

import com.sanam.ws.sanammobile.payload.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.util.Date;


/**
 * @author Ahmad Shekha
 * @since 4/11/2020
 */
public class ApiResponseBuilder {

    public ApiResponseBuilder() {
    }

    public static <R> ResponseEntity<ApiResponse<R>> ok() {
        return ResponseEntity.ok(successResponse());
    }

    public static <R> ResponseEntity<ApiResponse<R>> ok(R responseBody) {
        return ResponseEntity.ok(successResponse(responseBody));
    }

    public static <R> ResponseEntity<ApiResponse<R>> created(String entityPath) throws URISyntaxException {
        return ResponseEntity.created(new URI(entityPath)).build();
    }

    public static <R> ResponseEntity<ApiResponse<R>> created(R responseBody) throws URISyntaxException {
        return ResponseEntity.created(new URI("")).body(successResponse(responseBody));
    }

    public static <R> ResponseEntity<ApiResponse<R>> created(URI location, R responseBody) throws URISyntaxException {
        return location != null ? ResponseEntity.created(location).body(successResponse(responseBody)) :
                ResponseEntity.status(HttpStatus.CREATED).body(successResponse(responseBody));
    }

    /**
     * @param status
     * @param message
     * @return
     */
    public static ResponseEntity<Object> generic(HttpStatus status, String message) {
        return ResponseEntity
                .status(status)
                .body(errorResponse(null, message, null, new Object[0]));
    }


    private static <R> ApiResponse<R> successResponse() {
        return successResponse(null);
    }

    /**
     * create a response with a Succeeded status.
     *
     * @param responseBody Response Body
     * @param <R>
     *
     * @return Succeeded API Response
     */
    private static <R> ApiResponse<R> successResponse(R responseBody) {
        ApiResponse<R> response = new ApiResponse();
        response.setResponseStatus("SUCCEEDED");
        response.setDate(new Date());
        response.setResponseBody(responseBody);
        return response;
    }

    /**
     * create a response with a failed status
     *
     * @param error
     * @param arguments
     * @return
     */
//    private static <R> ApiResponse<R> errorResponse(WSBErrors error, Object[] arguments) {
//        return errorResponse(error.getCode(), error.getDescription(), arguments);
//    }

    /**
     * create a response with a failed status
     *
     * @param errorCode
     * @param errorMsg
     * @param arguments
     * @return
     */
    private static <R> ApiResponse<R> errorResponse(String errorCode, String errorMsg, Object[] arguments) {
        return errorResponse(errorCode, errorMsg, null, arguments);
    }

    /**
     * create a response with a failed status
     *
     * @param errorCode    Error Code
     * @param errorMsg     Error Message
     * @param errorDetails Error Details
     * @param arguments    Arguments
     *
     * @return Failed API Response
     */
    private static <R> ApiResponse<R> errorResponse(String errorCode, String errorMsg, String errorDetails, Object[] arguments) {

        ApiResponse<R> response = new ApiResponse<R>();

        response.setResponseStatus("FAILED");
        response.setDate(new Date());
        response.setErrorCode(errorCode);
        response.setErrorMessage(MessageFormat.format(errorMsg, arguments));
        response.setErrorDetails(errorDetails);

        return response;
    }
}
