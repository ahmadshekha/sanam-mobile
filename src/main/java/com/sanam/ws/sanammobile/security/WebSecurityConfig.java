package com.sanam.ws.sanammobile.security;

import com.sanam.ws.sanammobile.security.filter.JwtAuthenticationFilter;
import com.sanam.ws.sanammobile.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * This class implements Spring Security’s {@link WebSecurityConfigurerAdapter} interface. It provides default security
 * configurations and allows
 * other classes to extend it and customize the security configurations by overriding its methods.
 *
 * @author Ahmad Shekha
 * @since 4/2/2020
 */
@Configuration
// @EnableWebSecurity This is the primary spring security annotation that is used to enable web security in a project
@EnableWebSecurity
// @EnableGlobalMethodSecurity This is used to enable method level security based on annotations
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * To authenticate a User or perform various role-based checks, Spring security needs to load users details somehow.
     * For this purpose, It consists of an interface called UserDetailsService which has a single method that loads a user
     * based on username
     * {@code UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;}
     */
    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Autowired
    private AuthenticationEntryPointImpl authenticationEntryPointImpl;

    @Autowired
    private RestAccessDeniedHandler accessDeniedHandler;

    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter() {
        return new JwtAuthenticationFilter();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * The configure(HttpSecurity) method defines which URL paths should be secured and which
     * should not. Specifically, the / and /home paths are configured to not require any
     * authentication. All other paths must be authenticated.
     *
     * @param http
     *
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .cors()
                    .and()
                .csrf()
                    .disable()
                .exceptionHandling()
                    .accessDeniedHandler(accessDeniedHandler)
                    .authenticationEntryPoint(authenticationEntryPointImpl)
                    .and()
                .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                .authorizeRequests()
                .antMatchers("/",
                        "/favicon.ico",
                        "/**/*.png",
                        "/**/*.gif",
                        "/**/*.svg",
                        "/**/*.jpg",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js")
                        .permitAll()

                    .antMatchers("/api/auth/**")
                        .permitAll()

                    .antMatchers("/api/user/checkUsernameAvailability", "/api/user/checkEmailAvailability")
                        .permitAll()

                    .antMatchers(HttpMethod.GET, "/mobile/**", "/api/users/**")
                        .permitAll()

                    .anyRequest()
                        .authenticated();

        // Add our custom JWT security filter to validate the tokens with every request
        http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);


        //                        .and().formLogin().loginPage("/index.html")
        //                        .loginProcessingUrl("/perform_login").defaultSuccessUrl("/homepage.html", true)
        //                        .failureUrl("/index.html?error=true");

        // Secure the endpoins with HTTP Basic authentication
        //        http.cors().and().csrf().disable().exceptionHandling().accessDeniedHandler(accessDeniedHandler)
        //                .authenticationEntryPoint(authenticationEntryPointImpl).and()
        //                //HTTP Basic authentication
        //                .httpBasic().and().authorizeRequests().antMatchers(HttpMethod.GET, "/mobile/**").hasRole("USER");
    }
}