package com.sanam.ws.sanammobile.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sanam.ws.sanammobile.exception.ApiError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Ahmad Shekha
 * @since 4/8/2020
 */
@Component
public class RestAccessDeniedHandler implements AccessDeniedHandler {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void handle(final HttpServletRequest request, final HttpServletResponse response,
                       final AccessDeniedException accessDeniedException) throws IOException, ServletException {

        response.setContentType("application/json;charset=UTF-8");
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);

        ApiError forbiddenResponse = new ApiError(HttpStatus.FORBIDDEN);
        forbiddenResponse.setMessage("Access Denied");

        response.getWriter().write(objectMapper.writeValueAsString(forbiddenResponse));

    }
}
