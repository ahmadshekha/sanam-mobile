package com.sanam.ws.sanammobile.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sanam.ws.sanammobile.exception.ApiError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This to handle and customize the UnAuthorized Response.
 *
 * @author Ahmad Shekha
 * @since 4/6/2020
 */
@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint {

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * This is invoked when user tries to access a secured REST resource without supplying any credentials.
     * We should just send a 401 Unauthorized response because there is no 'login page' to redirect to
     *
     * @param request
     * @param response
     * @param authException
     *
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void commence(final HttpServletRequest request, final HttpServletResponse response,
                         final AuthenticationException authException) throws IOException, ServletException {

        response.setContentType("application/json;charset=UTF-8");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

        ApiError unauthorizedResponse = new ApiError(HttpStatus.UNAUTHORIZED);
        unauthorizedResponse.setMessage("UNAUTHORIZED request");

        response.getWriter().write(objectMapper.writeValueAsString(unauthorizedResponse));
    }
}
