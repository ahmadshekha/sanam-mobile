package com.sanam.ws.sanammobile.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.function.Function;

/**
 * This class is utility class will be used for generating a JWT after a user logs in successfully, and validating the JWT sent
 * in the Authorization header of the requests
 *
 * @author Ahmad Shekha
 * @since 4/11/2020
 */
@Component
public class JwtTokenProvider {

    private Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Value("${app.jwtExpirationInMs}")
    private int jwtExpirationInMs;

    /**
     * @param authentication
     *
     * @return
     */
    public String generateToken(final Authentication authentication) {

        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);

        return Jwts.builder()
                .setSubject(Long.toString(userPrincipal.getId()))
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    /**
     * @param token
     *
     * @return
     */
    public Long getUserIdFromJWT(final String token) {
        Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();

        logger.info("Expiration Date :: " + claims.getExpiration());
        return Long.parseLong(claims.getSubject());
    }

    /**
     * @param authToken
     *
     * @return
     */
    public boolean validateToken(final String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch(SignatureException ex) {
            logger.error("Invalid JWT signature");
        } catch(MalformedJwtException ex) {
            logger.error("Invalid JWT token");
        } catch(ExpiredJwtException ex) {
            logger.error("Expired JWT token");
        } catch(UnsupportedJwtException ex) {
            logger.error("Unsupported JWT token");
        } catch(IllegalArgumentException ex) {
            logger.error("JWT claims string is empty.");
        }
        return false;
    }
}
