package com.sanam.ws.sanammobile.exception;

/**
 * @author Ahmad Shekha
 * @since 4/11/2020
 */
public class ApiException extends RuntimeException {

    public ApiException(String message) {
        super(message);
    }

    public ApiException(String message, Throwable cause) {
        super(message, cause);
    }

}
