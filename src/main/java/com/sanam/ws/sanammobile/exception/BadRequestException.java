package com.sanam.ws.sanammobile.exception;

/**
 * @author Ahmad Shekha
 * @since 4/11/2020
 */
public class BadRequestException extends RuntimeException{

    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
