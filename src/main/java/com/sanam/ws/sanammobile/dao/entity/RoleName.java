package com.sanam.ws.sanammobile.dao.entity;

/**
 *
 */
public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
