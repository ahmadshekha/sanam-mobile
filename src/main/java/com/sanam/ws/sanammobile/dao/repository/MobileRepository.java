package com.sanam.ws.sanammobile.dao.repository;

import com.sanam.ws.sanammobile.dao.entity.Cover;
import com.sanam.ws.sanammobile.dao.entity.Mobile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 *
 */
@Repository
public interface MobileRepository extends JpaRepository<Mobile, Long> {

    List<Mobile> findMobilesByCategoryId(Long categoryId);

    Mobile findMobileByType(String type);
}
