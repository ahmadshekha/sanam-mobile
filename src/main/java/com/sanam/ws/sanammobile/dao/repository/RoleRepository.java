package com.sanam.ws.sanammobile.dao.repository;

import com.sanam.ws.sanammobile.dao.entity.Role;
import com.sanam.ws.sanammobile.dao.entity.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 *
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByRole(RoleName role);
}
