package com.sanam.ws.sanammobile.dao.repository;

import com.sanam.ws.sanammobile.dao.entity.Cover;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Ahmad Shekha
 * @since 12-01-2020
 */
@Repository
public interface CoverRepository extends JpaRepository<Cover, Long> {

    List<Cover> findCoversByCategoryId(Long categoryId);

    Cover findCoverByType(final String type);

    Optional<Cover> findByIdAndCategoryId(Long id, Long categoryId);
}
