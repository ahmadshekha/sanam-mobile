package com.sanam.ws.sanammobile.dao.repository;

import com.sanam.ws.sanammobile.dao.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Ahmad Shekha
 * @since 12-01-2020
 */
@Repository // <-- This tells Spring to bootstrap the repository during a component scan.
public interface CategoryRepository extends JpaRepository<Category, Long> {

}
