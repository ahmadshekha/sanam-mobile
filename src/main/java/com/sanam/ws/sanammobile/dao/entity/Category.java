package com.sanam.ws.sanammobile.dao.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

/**
 * @author Ahmad Shekha
 * @since 1/12/2020
 */
@Entity
@Table(name = "CATEGORIES")
public class Category implements Serializable {

    private Long id;
    private String englishName;
    private String arabicName;

    private Set<Cover> covers;
    private Set<Mobile> mobiles;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @NotNull
    @Column(name = "NAME_EN")
    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(final String englishName) {
        this.englishName = englishName;
    }

    @NotNull
    @Column(name = "NAME_AR")
    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(final String arabicName) {
        this.arabicName = arabicName;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "category", cascade = CascadeType.ALL)
    public Set<Cover> getCovers() {
        return covers;
    }

    public void setCovers(final Set<Cover> covers) {
        this.covers = covers;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "category", cascade = CascadeType.ALL)
    public Set<Mobile> getMobiles() {
        return mobiles;
    }

    public void setMobiles(final Set<Mobile> mobiles) {
        this.mobiles = mobiles;
    }
}
