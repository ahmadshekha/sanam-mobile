package com.sanam.ws.sanammobile.dao.entity;

import org.hibernate.annotations.NaturalId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Set;

/**
 * @author Ahmad Shekha
 * @since 3/23/2020
 */
@Entity
@Table(name = "ROLES")
public class Role {

    private int id;
    private RoleName role;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
        @Column(name = "ROLE_ID")
    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    @Enumerated(EnumType.STRING)
    @NaturalId
    @Column(name = "ROLE")
    public RoleName getRole() {
        return role;
    }

    public void setRole(final RoleName role) {
        this.role = role;
    }
}
