package com.sanam.ws.sanammobile.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sanam.ws.sanammobile.dao.audit.UserDateAudit;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Ahmad Shekha
 * @since 1/12/2020
 */
@Entity
@Table(name = "COVERS")
public class Cover extends UserDateAudit {

    private Long id;
    private String type;
    private Integer quantity;
    private Category category;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @NotNull
    @Column(name = "TYPE")
    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    @Column(name = "QUANTITY")
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(final Integer quantity) {
        this.quantity = quantity;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CATEGORY_ID", nullable = false)
    @JsonIgnore
    public Category getCategory() {
        return category;
    }

    public void setCategory(final Category category) {
        this.category = category;
    }
}
