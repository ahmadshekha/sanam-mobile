package com.sanam.ws.sanammobile.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * This is Spring configuration class to serve the static resources of our React app.
 * To allow cross origin requests from the react client.
 * Enabling CORS.
 *
 * @author Ahmad Shekha
 * @since 4/4/2020
 */
@EnableWebMvc
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    private final long MAX_AGE_SECS = 3600;

    /**
     * To allow cross origin requests from the react client.
     *
     * @param registry
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {

        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("HEAD", "OPTIONS", "GET", "POST", "PUT", "PATCH", "DELETE")
                .maxAge(MAX_AGE_SECS);
    }

    /**
     * addResourceHandlers: Add handlers to serve static resources such as images, js, and, css
     * files from specific locations under web application root, the classpath, and others.
     *
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        registry.addResourceHandler("/static/**").addResourceLocations("/app/build/static/");
        registry.addResourceHandler("/*.js").addResourceLocations("/app/build/");
        registry.addResourceHandler("/*.json").addResourceLocations("/app/build/");
        registry.addResourceHandler("/*.ico").addResourceLocations("/app/build/");
        registry.addResourceHandler("/index.html").addResourceLocations("/app/build/index.html");
    }

    /**
     * addViewControllers: Configure simple automated controllers pre-configured with the
     * response status code and/or a view to render the response body. This is useful in cases
     * where there is no need for custom controller logic -- e.g. render a home page, perform
     * simple site URL redirects, return a 404 status with HTML content, a 204 with no content,
     * and more.
     *
     * @param registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {

    }
}
