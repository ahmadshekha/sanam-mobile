package com.sanam.ws.sanammobile.controller;

import com.sanam.ws.sanammobile.api.ApiResponseBuilder;
import com.sanam.ws.sanammobile.dao.entity.Mobile;
import com.sanam.ws.sanammobile.payload.AddMobileRequest;
import com.sanam.ws.sanammobile.payload.EditMobileRequest;
import com.sanam.ws.sanammobile.payload.MobileResponse;
import com.sanam.ws.sanammobile.payload.ApiResponse;
import com.sanam.ws.sanammobile.service.SanamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Sanam Mobile Controller.
 *
 * @author Ahmad Shekha
 * @since 10/4/2019
 */
@RestController
@RequestMapping("/mobiles")
@CrossOrigin("http://localhost:8000")
@Validated
public class MobileController {

    private Logger logger = LoggerFactory.getLogger(MobileController.class);

    @Autowired
    private SanamService sanamSrv;

    /**
     * @param mobileId
     *
     * @return
     */
    @GetMapping("/{mobileId}")
    public ResponseEntity<ApiResponse<Mobile>> getMobileById(@PathVariable(value = "mobileId") Long mobileId) {

        logger.info("XXXXXXXXXXXX :: Get Mobile By Id");
        return ApiResponseBuilder.ok(sanamSrv.getMobileById(mobileId));
    }

    /**
     * @param categoryId
     *
     * @return
     */
    @GetMapping("/categories/{categoryId}")
    public ResponseEntity<ApiResponse<List<Mobile>>> getMobilesByCategory(@PathVariable(value = "categoryId") Long categoryId) {

        logger.info("XXXXXXXXXXXX :: getMobileByCategory");
        return ApiResponseBuilder.ok(sanamSrv.getMobileByCategory(categoryId));
    }

    /**
     * @param request
     *
     * @return
     *
     * @throws URISyntaxException
     */
    @PostMapping
    public ResponseEntity<ApiResponse<MobileResponse>> addMobile(@Valid @RequestBody AddMobileRequest request)
            throws URISyntaxException {

        Mobile mobile = sanamSrv.addMobile(request);
        URI location =
                ServletUriComponentsBuilder.fromCurrentRequest().path("/{mobileId}").buildAndExpand(mobile.getId()).toUri();

        return ApiResponseBuilder.created(location, new MobileResponse(mobile));
    }

    /**
     *
     * @param request
     * @return
     * @throws URISyntaxException
     */
    @PutMapping
    public ResponseEntity<ApiResponse<MobileResponse>> editMobile(@Valid @RequestBody EditMobileRequest request)
            throws URISyntaxException {

        Mobile mobile = sanamSrv.editMobile(request);
        URI location =
                ServletUriComponentsBuilder.fromCurrentRequest().path("/{mobileId}").buildAndExpand(mobile.getId()).toUri();

        return ApiResponseBuilder.created(location, new MobileResponse(mobile));
    }
}
