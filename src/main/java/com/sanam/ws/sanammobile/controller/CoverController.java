package com.sanam.ws.sanammobile.controller;

import com.sanam.ws.sanammobile.api.ApiResponseBuilder;
import com.sanam.ws.sanammobile.dao.entity.Cover;
import com.sanam.ws.sanammobile.payload.AddCoverRequest;
import com.sanam.ws.sanammobile.payload.AddCoverResponse;
import com.sanam.ws.sanammobile.payload.ApiResponse;
import com.sanam.ws.sanammobile.service.SanamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * @author Ahmad Shekha
 * @since 4/11/2020
 */
@RestController
@RequestMapping("/covers")
@CrossOrigin("http://localhost:8000")
@Validated
public class CoverController {

    private Logger logger = LoggerFactory.getLogger(MobileController.class);

    @Autowired
    private SanamService sanamSrv;

    /**
     * @param coverId Cover Id
     *
     * @return
     */
    @GetMapping("/{cover_id}")
    public ResponseEntity<ApiResponse<Cover>> getCoverById(@PathVariable(value = "cover_id") Long coverId) {
        logger.info("XXXXXXXXXXXX :: getMobileCoversByCategory");

        return ApiResponseBuilder.ok(sanamSrv.getCoverById(coverId));
    }

    /**
     * @param categoryId
     *
     * @return
     */
    @GetMapping("/categories/{category_id}")
    public ResponseEntity<ApiResponse<List<Cover>>> getMobileCoversByCategory(
            @PathVariable(value = "category_id") Long categoryId) {
        logger.info("XXXXXXXXXXXX :: getMobileCoversByCategory");

        return ApiResponseBuilder.ok(sanamSrv.getMobileCoversByCategory(categoryId));
    }

    /**
     * @param request
     *
     * @return
     *
     * @throws URISyntaxException
     */
    @PostMapping
    public ResponseEntity<ApiResponse<AddCoverResponse>> addCover(@Valid @RequestBody AddCoverRequest request)
            throws URISyntaxException {

        Cover cover = sanamSrv.addCover(request);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{cover_id}").buildAndExpand(cover.getId()).toUri();

        return ApiResponseBuilder.created(location, new AddCoverResponse(cover));
    }
}
