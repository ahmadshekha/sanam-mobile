package com.sanam.ws.sanammobile.controller;

import com.sanam.ws.sanammobile.api.ApiResponseBuilder;
import com.sanam.ws.sanammobile.dao.entity.User;
import com.sanam.ws.sanammobile.payload.ApiResponse;
import com.sanam.ws.sanammobile.payload.JwtAuthenticationResponse;
import com.sanam.ws.sanammobile.payload.LoginRequest;
import com.sanam.ws.sanammobile.payload.SignUpRequest;
import com.sanam.ws.sanammobile.payload.SignUpResponse;
import com.sanam.ws.sanammobile.service.AuthService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;


/**
 * @author Ahmad Shekha
 * @since 3/24/2020
 */
@RestController
@RequestMapping("/api/auth")
@CrossOrigin("http://localhost:8000")
@Validated
public class AuthController {

    private Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    private AuthService authService;

    /**
     * @param loginRequest
     *
     * @return
     */
    @PostMapping("/signin")
    public ResponseEntity<ApiResponse<JwtAuthenticationResponse>> login(@Valid @RequestBody LoginRequest loginRequest) {

        logger.info("User Trying to login");

        return ApiResponseBuilder.ok(authService.login(loginRequest));
    }

    /**
     *
     * @param signUpRequest
     * @return
     * @throws URISyntaxException
     */
    @PostMapping("/signup")
    public ResponseEntity<ApiResponse<SignUpResponse>> registerUser(@Valid @RequestBody SignUpRequest signUpRequest)
            throws URISyntaxException {

        User user = authService.registerUser(signUpRequest);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/{username}")
                .buildAndExpand(user.getUsername()).toUri();

        return ApiResponseBuilder.created(location, new SignUpResponse(user));
    }
}
