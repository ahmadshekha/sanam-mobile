package com.sanam.ws.sanammobile.service;

import com.sanam.ws.sanammobile.dao.entity.Category;
import com.sanam.ws.sanammobile.dao.repository.CategoryRepository;
import com.sanam.ws.sanammobile.dao.repository.CoverRepository;
import com.sanam.ws.sanammobile.dao.repository.MobileRepository;
import com.sanam.ws.sanammobile.dao.entity.Cover;
import com.sanam.ws.sanammobile.dao.entity.Mobile;
import com.sanam.ws.sanammobile.exception.BadRequestException;
import com.sanam.ws.sanammobile.exception.EntityNotFoundException;
import com.sanam.ws.sanammobile.payload.AddCoverRequest;
import com.sanam.ws.sanammobile.payload.AddMobileRequest;
import com.sanam.ws.sanammobile.payload.EditMobileRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Ahmad Shekha
 * @since 1/12/2020
 */
@Service
public class SanamService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final CategoryRepository categoryRepository;
    private final CoverRepository coverRepository;
    private final MobileRepository mobileRepository;

    public SanamService(final CategoryRepository categoryRepository, final CoverRepository coverRepository,
                        final MobileRepository mobileRepository) {
        this.categoryRepository = categoryRepository;
        this.coverRepository = coverRepository;
        this.mobileRepository = mobileRepository;
    }

    public List<Cover> getMobileCoversByCategory(Long categoryId) {

        List<Cover> covers = coverRepository.findCoversByCategoryId(categoryId);

        if(covers == null || covers.isEmpty()) {
            throw new EntityNotFoundException(Cover.class, "categoryId", categoryId.toString());
        }

        return covers;
    }

    public List<Mobile> getMobileByCategory(final Long categoryId) {


        List<Mobile> mobiles = mobileRepository.findMobilesByCategoryId(categoryId);

        if(mobiles == null || mobiles.isEmpty()) {
            throw new EntityNotFoundException(Mobile.class, "categoryId", categoryId.toString());
        }

        mobiles.forEach(mobile -> System.out.println(mobile.getCreatedAt()));

        return mobiles;
    }

    /**
     * @param request
     *
     * @return
     */
    public Mobile addMobile(final AddMobileRequest request) {

        long categoryId = request.getCategory();
        Category category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new EntityNotFoundException(Category.class, "categoryId", String.valueOf(categoryId)));

        Mobile mobile = mobileRepository.findMobileByType(request.getType());

        if(mobile != null) {
            throw new BadRequestException("Mobile Type is already exist");
        }

        category.setCovers(null);
        category.setMobiles(null);
        mobile = new Mobile();

        mobile.setType(request.getType());
        mobile.setCategory(category);
        mobile.setQuantity(request.getQuantity());
        mobileRepository.save(mobile);

        return mobile;
    }

    /**
     * @param mobileId
     *
     * @return
     */
    public Mobile getMobileById(final Long mobileId) {
        return mobileRepository.findById(mobileId)
                .orElseThrow(() -> new EntityNotFoundException(Mobile.class, "mobile_id", mobileId.toString()));
    }

    /**
     *
     * @param request
     * @return
     */
    public Mobile editMobile(final EditMobileRequest request) {
        Mobile mobile = mobileRepository.findById(request.getMobileId())
                .orElseThrow(() -> new EntityNotFoundException(Mobile.class, "mobile_id", String.valueOf(request.getMobileId())));

        mobile.setType(request.getType());
        mobile.setQuantity(request.getQuantity());

        if(request.getCategory() != 0) {
            Category category = categoryRepository.findById(request.getCategory()).orElseThrow(
                    () -> new EntityNotFoundException(Mobile.class, "category_id", String.valueOf(request.getCategory())));
            mobile.setCategory(category);
        }

        mobileRepository.save(mobile);

        return mobile;
    }

    /**
     * @param coverId
     *
     * @return
     */
    public Cover getCoverById(final Long coverId) {
        return coverRepository.findById(coverId)
                .orElseThrow(() -> new EntityNotFoundException(Cover.class, "cover_id", coverId.toString()));
    }

    /**
     * @param request
     *
     * @return
     */
    public Cover addCover(final AddCoverRequest request) {

        long categoryId = request.getCategory();
        Category category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new EntityNotFoundException(Category.class, "category_id", String.valueOf(categoryId)));

        Cover cover = coverRepository.findCoverByType(request.getType());

        if(cover != null) {
            throw new BadRequestException("Cover Type is already exist");
        }

        category.setCovers(null);
        category.setMobiles(null);
        cover = new Cover();

        cover.setType(request.getType());
        cover.setCategory(category);
        cover.setQuantity(request.getQuantity());
        coverRepository.save(cover);

        return cover;
    }
}
