package com.sanam.ws.sanammobile.service;

import com.sanam.ws.sanammobile.dao.entity.User;
import com.sanam.ws.sanammobile.dao.repository.UserRepository;
import com.sanam.ws.sanammobile.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * the custom UserDetailsService which loads a user’s data given its username.
 *
 * @author Ahmad Shekha
 * @since 4/11/2020
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
        // Let people login with either username or email
        User user = userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail)
                .orElseThrow(() -> new UsernameNotFoundException("User not found with username or email : " + usernameOrEmail));

        return UserPrincipal.create(user);
    }

    /**
     * This method is used by JWTAuthenticationFilter.
     *
     * @param id
     *
     * @return
     */
    @Transactional
    public UserDetails loadUserById(Long id) {
        User user =
                userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("User not found with id : " + id));

        return UserPrincipal.create(user);
    }
}
