package com.sanam.ws.sanammobile.service;

import com.sanam.ws.sanammobile.dao.entity.Role;
import com.sanam.ws.sanammobile.dao.entity.RoleName;
import com.sanam.ws.sanammobile.dao.entity.User;
import com.sanam.ws.sanammobile.dao.repository.RoleRepository;
import com.sanam.ws.sanammobile.dao.repository.UserRepository;
import com.sanam.ws.sanammobile.exception.ApiException;
import com.sanam.ws.sanammobile.exception.BadRequestException;
import com.sanam.ws.sanammobile.payload.JwtAuthenticationResponse;
import com.sanam.ws.sanammobile.payload.LoginRequest;
import com.sanam.ws.sanammobile.payload.SignUpRequest;
import com.sanam.ws.sanammobile.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

/**
 * @author Ahmad Shekha
 * @since 3/24/2020
 */
@Service
public class AuthService {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

    /**
     * @param loginRequest
     *
     * @return
     */
    public JwtAuthenticationResponse login(final LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsernameOrEmail(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        return new JwtAuthenticationResponse(jwt);
    }

    public User registerUser(final SignUpRequest signUpRequest) {

        if(userRepository.existsByUsername(signUpRequest.getUsername())) {
            throw new BadRequestException("Username is already taken!");
        }

        if(userRepository.existsByEmail(signUpRequest.getEmail())) {
            throw new BadRequestException("Email Address already in use!");
        }

        // Creating user's account
        User user = new User(signUpRequest.getUsername(), signUpRequest.getEmail(), signUpRequest.getPassword(),
                signUpRequest.getFirstName(), signUpRequest.getLastName());

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        Role userRole = roleRepository.findByRole(RoleName.ROLE_USER)
                .orElseThrow(() -> new ApiException("User Role not set."));

        user.setRoles(Collections.singleton(userRole));

        User result = userRepository.save(user);

        return result;
    }
}
