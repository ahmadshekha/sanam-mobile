import React, { useState } from "react";
import {
  Button,
  Card,
  CardContent,
  CircularProgress,
  Container,
  makeStyles,
  TextField,
  Typography
} from "@material-ui/core";
import { useHistory } from "react-router";
// import { AuthService } from "../../services/auth/AuthService";

export function Login() {
  const classes = useStyles();

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const history = useHistory();

  const signIn = event => {
    event.preventDefault();

    // AuthService.authenticate(() => setRedirectToReferrer(true));

    if (!(username === "ahmad" && password === "passw0rd")) {
      setError(true);
    }

    setLoading(true);
    console.log("Yay, Logged in");
    history.push("/");
  };

  return (
    <Container maxWidth="xs">
      <div className={classes.paper}>
        <Card className={classes.card}>
          <CardContent>
            <Typography variant="h4" className={classes.title}>
              Sanam.com
            </Typography>
            <div className={classes.space} />
            <form className={classes.form}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                type="username"
                autoComplete="email"
                autoFocus
                value={username}
                onChange={event => setUsername(event.target.value)}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                value={password}
                onChange={event => setPassword(event.target.value)}
              />
              {error && (
                <Typography color="error" variant="subtitle2">
                  Invalid Username or Password
                </Typography>
              )}
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={signIn}
              >
                {loading ? (
                  <CircularProgress style={{ color: "#81ed46" }} size={20} />
                ) : (
                  "Sign In"
                )}
              </Button>
            </form>
          </CardContent>
        </Card>
      </div>
    </Container>
  );
}

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(18),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },

  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  card: {
    maxWidth: 800,
    minWidth: "50%"
  },

  space: {
    height: theme.spacing(3)
  },

  title: {
    fontWeight: 300,
    color: "#81ed46",
    textAlign: "center"
  }
}));
