import React from "react";
import { makeStyles, Container, Grid } from "@material-ui/core";
import { useHistory } from "react-router";
import CustomCarousel from "../carousel/CustomCarousel";
import CustomCard from "../card/Card";
import categories from "../constant/categories";
import "./Home.css";

const useStyles = makeStyles(theme => ({
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8)
  }
}));

const Home = () => {
  const classes = useStyles();
  const history = useHistory();

  const handleClick = props => {
    history.push(props.to);
  };

  return (
    <main>
      <CustomCarousel />
      <h1 className="page-title">Home</h1>
      <Container className={classes.cardGrid}>
        <Grid container spacing={4}>
          {categories.map(category => (
            <CustomCard
              id={category.id}
              key={category.id}
              name={category.name}
              img={category.imgURL}
              desc={category.desc}
              to={category.to}
              handleClick={handleClick}
            />
          ))}
        </Grid>
      </Container>
    </main>
  );
};

export default Home;
