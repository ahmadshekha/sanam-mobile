import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useParams } from "react-router-dom";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import { TableRow, Container } from "@material-ui/core";

const columns = [
  {
    id: "type",
    label: "Type",
    minWidth: 170
  },
  {
    id: "quantity",
    label: "Quantity",
    minWidth: 170,
    format: value => value.toLocaleString()
  },
  {
    id: "actions",
    label: "Actions",
    minWidth: 170,
    align: "center"
  }
];

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    margin: theme.spacing(3, 20, 3)
  },
  tableCell: {
    backgroundColor: "#343a40",
    color: "#81ed46",
    borderBottom: "1px solid #81ed46"
  }
}));

const CoverList = () => {
  const classes = useStyles();
  const [covers, setCovers] = useState([]);
  let { topicId, category } = useParams();

  useEffect(() => {
    getCovers();
  }, []);

  const getCovers = async () => {
    await fetch(
      `http://localhost:8083/sanam/mobile/categories/${topicId} /covers`
    )
      .then(res => res.json())
      .then(data => {
        setCovers(data);
        console.log(data[0].type);
      })
      .catch(console.log);
  };

  return (
    <Container>
      <h1 className="page-title">{category}</h1>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table" className={classes.table}>
          <TableHead className={classes.table}>
            <TableRow className={classes.table}>
              {columns.map(column => (
                <TableCell
                  className={classes.tableCell}
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {covers.map(row => {
              console.log(row.id);
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                  {columns.map(column => {
                    const value = row[column.id];
                    return (
                      <TableCell className={classes.tableCell} key={column.id}>
                        {value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  );
};

export default CoverList;
