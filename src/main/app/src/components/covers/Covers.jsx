import React from "react";
import { useHistory } from "react-router";
import { useRouteMatch } from "react-router-dom";
import { makeStyles, Container, Grid } from "@material-ui/core";
import CustomCard from "../card/Card";
import mobileTypes from "../constant/mobileTypes";

const useStyles = makeStyles(theme => ({
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8)
  }
}));

const Covers = () => {
  const classes = useStyles();
  const { url } = useRouteMatch();
  const history = useHistory();

  const handleClick = props => {
    console.log(props);

    history.push(props.to);
  };

  return (
    <main>
      <h1 className="page-title">Covers</h1>
      <Container className={classes.cardGrid}>
        <Grid container spacing={4}>
          {mobileTypes.map(card => (
            <CustomCard
              id={card.id}
              key={card.id}
              name={card.title}
              img={card.imgURL}
              desc={card.desc}
              to={`${url}/${card.id}/${card.title}`}
              handleClick={handleClick}
            />
          ))}
        </Grid>
      </Container>
    </main>
  );
};

export default Covers;
