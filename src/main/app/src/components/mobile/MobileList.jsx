import React, { useState, useEffect } from "react";
import { Container, ListGroup } from "react-bootstrap";
import { useParams } from "react-router-dom";

const MobileList = () => {
  const [mobiles, setMobiles] = useState([]);
  let { topicId, category } = useParams();

  useEffect(() => {
    getMobiles();
  }, []);

  const getMobiles = async () => {
    await fetch(
      `http://localhost:8083/sanam/mobile/categories/${topicId} /mobiles`
    )
      .then(res => res.json())
      .then(data => {
        setMobiles(data);
        console.log(data[0].type);
      })
      .catch(console.log);
  };

  return (
    <Container>
      <h1 className="page-title">{category}</h1>
      <ListGroup>
        {mobiles.map(mobile => (
          <ListGroup.Item key={mobile.id} id={mobile.id}>
            {mobile.type}
          </ListGroup.Item>
        ))}
      </ListGroup>
    </Container>
  );
};

export default MobileList;
