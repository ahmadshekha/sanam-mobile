const MobileTypes = [
  {
    id: 1,
    title: "Iphone",
    imgURL: "https://wallpaperaccess.com/full/1377276.jpg",
    desc: "Go To Iphone Mobiles"
  },
  {
    id: 2,
    title: "Samsung",
    imgURL:
      "https://i.pinimg.com/originals/f6/7f/3c/f67f3cfe871dbc8ca040abacba4c2d16.png",
    desc: "Go To Samsung Mobiles"
  },
  {
    id: 3,
    title: "Huawei",
    imgURL:
      "https://i.pinimg.com/474x/be/2e/07/be2e07f7ed7f13431ddb05d79ebff0f0.jpg",
    desc: "Go To Huawei Mobiles"
  }
];

export default MobileTypes;
