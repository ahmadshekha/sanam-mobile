const Categories = [
  {
    id: 1,
    name: "Mobiles",
    imgURL: "assets/apple-logo_1.png",
    desc: "Go To Mobiles Category",
    to: "/mobiles"
  },
  {
    id: 2,
    name: "Covers",
    imgURL: "assets/ahmad.jpg",
    desc: "Go To Covers Category",
    to: "/covers"
  },
  {
    id: 3,
    name: "Accessories",
    imgURL: "assets/ahmad.jpg",
    desc: "Go To Accessories Category",
    to: "/accessories"
  }
];

export default Categories;
