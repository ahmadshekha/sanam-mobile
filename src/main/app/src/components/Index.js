import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import CustomNavbar from "./navbar/CustomNavbar";
import Mobile from "./mobile/Mobile";
import MobileList from "./mobile/MobileList";
import Covers from "./covers/Covers";
import CoverList from "./covers/CoverList";
import Home from "./home/Home";
import Accessories from "./accessories/Accessories";

import { makeStyles, Typography, Link, Container } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexDirection: "column",
    minHeight: "100vh"
  },
  main: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(2)
  },
  footer: {
    backgroundColor: "#343a40",
    textAlign: "center",
    color: "#81ed46",
    padding: theme.spacing(3, 2),

    marginTop: theme.spacing(8),

    fontFamily: ['"Merriweather"', '"serif"']
  }
}));

function Index() {
  const classes = useStyles();

  return (
    <div>
      <CustomNavbar />

      <BrowserRouter>
        <Switch>
          <Route path={"/covers/:topicId/:category"} component={CoverList} />
          <Route path={"/mobiles/:topicId/:category"} component={MobileList} />
          <Route path="/accessories" component={Accessories} />
          <Route path="/mobiles" component={Mobile} />
          <Route path="/covers" component={Covers} />
          <Route path="/" component={Home} />
        </Switch>
      </BrowserRouter>

      <footer className={classes.footer}>
        <Container maxWidth="sm">
          <Typography variant="body1">
            My sticky footer can be found here.
          </Typography>
          <Copyright />
        </Container>
      </footer>
    </div>
  );
}

function Copyright() {
  return (
    <Typography variant="body2" align="center">
      {"Copyright © "}
      <Link color="inherit" href="http://localhost:8000/">
        Sanam.com
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

export default Index;
