import React from "react";
import "./Card.css";
import {
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Grid,
  Typography,
  makeStyles,
  Button
} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    backgroundColor: "#343a40",
    color: "#81ed46"
  },
  cardMedia: {
    paddingTop: "56.25%" // 16:9
  },
  cardContent: {
    flexGrow: 1
  },
  btn: {
    margin: theme.spacing(3, 0, 2),
    background: "#81ed46"
  }
}));

const CustomCard = props => {
  const classes = useStyles();
  return (
    <Grid item xs={12} sm={6} md={4}>
      <Card className={classes.card}>
        <CardMedia
          className={classes.cardMedia}
          image={props.img}
          title="Image title"
        />
        <CardContent className={classes.cardContent}>
          <Typography gutterBottom variant="h5" component="h2">
            {props.name}
          </Typography>
          <Typography>{props.desc}</Typography>
        </CardContent>
        <CardActions>
          <Button
            fullWidth
            variant="contained"
            className={classes.btn}
            onClick={() => props.handleClick(props)}
          >
            View
          </Button>
        </CardActions>
      </Card>
    </Grid>
  );
};

export default CustomCard;
