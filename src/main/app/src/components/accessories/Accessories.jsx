import React from "react";
import { makeStyles, Container, Grid } from "@material-ui/core";
import CustomCard from "../card/Card";

const useStyles = makeStyles(theme => ({
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8)
  }
}));

const cards = [1, 2, 3];

const Accessories = () => {
  const classes = useStyles();
  return (
    <main>
      <Container className={classes.cardGrid}>
        <Grid container spacing={4}>
          {cards.map(card => (
            <CustomCard
              key={card}
              name="Heading"
              img="https://source.unsplash.com/random"
              desc="This is a media card. You can use this section to describe
                    the content."
            />
          ))}
        </Grid>
      </Container>
    </main>
  );
};

export default Accessories;
