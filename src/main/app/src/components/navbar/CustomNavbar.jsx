import React from "react";
import { Navbar, Nav } from "react-bootstrap";
import "./CustomNavbar.css";

const CustomNavbar = () => {
  return (
    <Navbar expand="lg" bg="dark" variant="dark" sticky="top">
      <Navbar.Brand href="/">Sanam.com</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse className="justify-content-center">
        <Nav activeKey="/">
          <Nav.Item>
            <Nav.Link eventKey={1} href="/">
              Home
            </Nav.Link>
          </Nav.Item>

          <Nav.Item>
            <Nav.Link eventKey={2} href="/mobiles">
              Mobile
            </Nav.Link>
          </Nav.Item>

          <Nav.Item>
            <Nav.Link eventKey={3} href="/covers">
              Covers
            </Nav.Link>
          </Nav.Item>

          <Nav.Item>
            <Nav.Link eventKey={4} href="/accessories">
              Accessories
            </Nav.Link>
          </Nav.Item>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default CustomNavbar;
