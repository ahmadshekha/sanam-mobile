import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Index from "./components/Index";

import { Login, SignUp } from "./components/auth";
import { AuthService } from "./services/auth/AuthService";

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/registration" component={SignUp} />
        <Route path="/login" component={Login} />
        <Route path="/" component={Index} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
